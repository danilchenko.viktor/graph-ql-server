import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateUserDto {
  @Field()
  name: string;
  @Field()
  email: string;
  @Field()
  address: string;
  @Field()
  birthday: string;
  @Field()
  bio?: string;
}
