import { Field, ObjectType, ID } from '@nestjs/graphql';
import { Entity, ManyToOne, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Post } from 'src/posts/entities/post.entity';

@ObjectType()
@Entity()
export class Comment {
  @Field(() => ID)
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Field()
  @Column()
  userId: string;

  @Field()
  @Column()
  name: string;

  @Field()
  @Column()
  content: string;

  @Field(() => Post)
  @ManyToOne(() => Post, (post) => post.comments)
  post: Post;
}
