import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { Comment } from './entities/comment.entity';
import { PostsService } from 'src/posts/posts.service';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(Comment)
    private readonly commentRepository: Repository<Comment>,
    private readonly postsService: PostsService,
  ) {}

  async create(createCommentDto: CreateCommentDto) {
    const comment = new Comment();
    comment.userId = createCommentDto.userId;
    comment.name = createCommentDto.name;
    comment.content = createCommentDto.content;

    const post = await this.postsService.findOne(createCommentDto.postId);

    comment.post = post;

    return this.commentRepository.save(comment);
  }

  findAll(postId?: string) {
    if (postId) {
      return this.commentRepository.find({
        where: { post: { id: postId } },
      });
    }
    return this.commentRepository.find();
  }

  findOne(id: number) {
    return `This action returns a #${id} comment`;
  }

  findByPost(postId: string) {
    return this.commentRepository.find({
      where: { post: { id: postId } },
    });
  }

  update(id: number, updateCommentDto: UpdateCommentDto) {
    return `This action updates a #${id} comment`;
  }

  remove(id: number) {
    return `This action removes a #${id} comment`;
  }
}
