export class CreateCommentDto {
  userId: string;
  postId: string;
  name: string;
  content: string;
}
