import { Field, InputType } from '@nestjs/graphql';
import { PartialType } from '@nestjs/mapped-types';
import { CreateCommentDto } from './create-comment.dto';

export class UpdateCommentDto extends PartialType(CreateCommentDto) {}

@InputType()
export class UpdatePostInput implements Partial<CreateCommentDto> {
  @Field()
  id: string;

  @Field({ nullable: true })
  content?: string;
}
