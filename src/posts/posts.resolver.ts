import {
  Args,
  Query,
  Resolver,
  Parent,
  ResolveField,
  Mutation,
} from '@nestjs/graphql';
import { Post } from './entities/post.entity';
import { PostsService } from './posts.service';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostInput } from './dto/update-post.dto';
import { CommentsService } from '../comments/comments.service';

@Resolver(() => Post)
export class PostsResolver {
  constructor(
    private readonly postsService: PostsService,
    private readonly commentsService: CommentsService,
  ) {}

  @Query(() => [Post])
  async posts(): Promise<Post[]> {
    const posts = await this.postsService.findAll();
    if (!posts) {
      return [];
    }
    return posts;
  }

  @Query(() => Post)
  post(@Args('id') id: string) {
    return this.postsService.findOne(id);
  }

  @ResolveField()
  async comments(@Parent() post: Post) {
    const comments = await this.commentsService.findByPost(post.id);
    if (!comments) {
      return [];
    }
    return comments;
  }

  @Mutation(() => Post)
  createPost(@Args('createPostDto') createPostDto: CreatePostDto) {
    return this.postsService.create(createPostDto);
  }

  @Mutation(() => Post)
  updatePost(@Args('updatePostInput') updatePostInput: UpdatePostInput) {
    return this.postsService.update(updatePostInput.id, updatePostInput);
  }

  @Mutation(() => Boolean)
  removePost(@Args('id') id: string) {
    return this.postsService.remove(id);
  }
}
