import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePostDto } from './dto/create-post.dto';
import { UpdatePostDto } from './dto/update-post.dto';
import { Post } from './entities/post.entity';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class PostsService {
  constructor(
    @InjectRepository(Post)
    private readonly postRepository: Repository<Post>,
    private readonly usersService: UsersService,
  ) {}

  async create(createPostDto: CreatePostDto) {
    const post = new Post();
    post.title = createPostDto.title;
    post.content = createPostDto.content;

    const author = await this.usersService.findOne(createPostDto.authorId);

    post.author = author;

    return this.postRepository.save(post);
  }

  findAll(userId?: string) {
    if (userId) {
      return this.postRepository.find({
        where: { author: { id: userId } },
        relations: ['author'],
      });
    }

    return this.postRepository.find({
      relations: ['author'],
    });
  }

  findOne(id: string) {
    return this.postRepository.findOne(id);
  }

  findByAuthor(authorId: string) {
    return this.postRepository.find({
      relations: ['author'],
      where: { author: { id: authorId } },
    });
  }

  async update(id: string, updatePostDto: UpdatePostDto) {
    const post = await this.postRepository.findOne(id);
    post.title = updatePostDto.title;
    post.content = updatePostDto.content;
    return this.postRepository.save(post);
  }

  remove(id: string) {
    return this.postRepository.delete(id);
  }
}
