import { Field, InputType } from '@nestjs/graphql';
import { PartialType } from '@nestjs/mapped-types';
import { CreatePostDto } from './create-post.dto';

export class UpdatePostDto extends PartialType(CreatePostDto) {}

@InputType()
export class UpdatePostInput extends CreatePostDto {
  @Field()
  id: string;

  @Field()
  content: string;
}
